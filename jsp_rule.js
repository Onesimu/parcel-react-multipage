const matchHelper = require('posthtml-match-helper');
function getExp(attributeValue){
    const exp = attributeValue.slice(2, attributeValue.length - 1)
    return exp.replace(/not empty /g, '').replace(/ empty /g,' !')
        .replace(/ and /g,' && ').replace(/ or /g,' || ').replace(/ eq /g,' == ').replace(/ ne /g,' != ')
        // .replace(/ /g,'!').replace(/ /g,'!').replace(/ /g,'!')       
}

module.exports = function (options) {
    options = options || {};

    return function renameTags(tree) {
        var keys = Object.keys(options);
        var matchers = keys.map(matchHelper);
        keys.forEach(function(key, i) {
            tree.match(matchers[i], function(node) {
                if(node.tag == "c:if" || node.tag == "c:when"){
                    const t = node.attrs['test']
                    node.attrs['v-if'] = getExp(t)
                    delete node.attrs['test']
                }
                if(node.tag == "c:otherwise"){
                    node.attrs = {['v-else'] : ''}
                }
                node.tag = options[key];
                return node;
            });
        });
          tree.walk(node => {
                node.attrs && Object.keys(node.attrs).forEach(function (attribute) {
                    var attributeValue = node.attrs[attribute];
                    if(attributeValue.startsWith('${') && attributeValue.endsWith('}')){
                        node.attrs[':' + attribute] = getExp(attributeValue)
                        delete node.attrs[attribute]
                    }

                })
              return node
          })
    };
};
