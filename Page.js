import React from "react";

const Page = () => (
  <div className="longBannerWrap">
    <img
      className="longBanner"
      src="./images/img_12699_0_0.png"
      data-src="./images/img_12699_0_0.png"
      alt="longBanner"
    />
    <div className="container">
      <div className="color" />
    </div>
    <div className="layerWrap">
      <img
        className="layer"
        src="./images/img_12699_0_1.png"
        data-src="./images/img_12699_0_1.png"
        alt="layer"
      />
      <div className="outer">
        <span className="word">*</span>
        <div className="colorDiv" />
      </div>
      <span className="studentName">学生姓名</span>
      <div className="block">
        <div className="titleWrap">
          <span className="title">请填写学生姓名</span>
        </div>
      </div>
      <div className="group">
        <span className="text">*</span>
        <div className="color_2" />
        <span className="contactInformation">联系方式</span>
      </div>
      <div className="container_2">
        <div className="titleWrap_2">
          <span className="title_2">请填写您的手机号码</span>
        </div>
      </div>
      <div className="outer_2">
        <div className="group_2">
          <img
            className="jiechulahei"
            src="./images/img_12699_0_2.png"
            data-src="./images/img_12699_0_2.png"
            alt="jiechulahei"
          />
          <span className="title_3">请输入验证码</span>
        </div>
        <div className="sendVerificationCodeWrap">
          <span className="sendVerificationCode">发送验证码</span>
        </div>
      </div>
      <div className="gradeWrap">
        <span className="grade">年级</span>
      </div>
      <div className="group_3">
        <div className="firstGradeWrap">
          <span className="firstGrade">一年级</span>
        </div>
        <div className="secondGradeWrap">
          <span className="secondGrade">二年级</span>
        </div>
        <div className="thirdGradeWrap">
          <span className="thirdGrade">三年级</span>
        </div>
      </div>
      <div className="container_3">
        <div className="fourthGradeWrap">
          <span className="fourthGrade">四年级</span>
        </div>
        <div className="fifthGradeWrap">
          <span className="fifthGrade">五年级</span>
        </div>
        <div className="gradeSixWrap">
          <span className="gradeSix">六年级</span>
        </div>
      </div>
      <div className="submitOuter">
        <div className="submitWrap">
          <span className="submit">提交</span>
        </div>
      </div>
    </div>
  </div>
);

export default Page;
