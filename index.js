import React from "react";
import ReactDOM from "react-dom";
import Page from './Page.js'

var mountNode = document.getElementById("app");
ReactDOM.render(<Page />, mountNode);