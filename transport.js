const path = require('path');
const CWD = require('process').cwd();
const globby = require('globby');
const rimraf = require('rimraf');
const process = require('process');

// import {readFileSync, writeFileSync} from 'fs';
const fs = require('fs')
// import posthtml from 'posthtml';
// import removeTags from 'posthtml-remove-tags';
const parser = require('posthtml-parser')
const extractReactComponents = require("html-to-react-components");

function clean(files) {
    console.log(files)
    if (files && typeof files === 'string') {
        files = [files];
    } else if (!Array.isArray(files) || !files.length) {
        return;
    }
    globby.sync(files, { onlyFiles: false, expandDirectories: false }).map(name => {
        const file = path.resolve(name);
        if (file !== CWD) {
            rimraf.sync(file);
        }
    });
}

module.exports = bundler => {
    let config;
    try {
        config = require(path.resolve(CWD, 'package.json'));
    } catch (e) {
        return;
    }
    if (!config) {
        return;
    }
    // return
    clean(config.parcelClean);
    // bundler.on('bundled', (bundler) => {
    //   // bundler 包含所有资源和 bundle，如需了解更多请查看文档
    //   console.log('bundled', bundler)
    // });
    bundler.on('buildStart', entryPoints => {
      // 做一些操作……
      console.log('buildStart', entryPoints)
      const html = fs.readFileSync(entryPoints[0], 'utf8');
      
      const jsx = extractReactComponents(html,  {
        componentType: "stateless",
        output: {path: './'}        // moduleType: false
      }
    );
      // console.log(jsx)
      // console.log(html)
      // const content = html.match(/<body>([\s\S]*)<\/body>/gmi)
      // console.log(content[0])
      // const indexJsPath = entryPoints[0].replace('.html', '.js')
      // const jsx = content[0].replace('<body>\n', '').replace('\n</body>', '')
      //   .replace(/class/g, 'className')
      // const jsTpl = `import React from "react";
      //   import ReactDOM from "react-dom";

      //   function App(props) {
      //       return ${jsx}
      //   }

      //   var mountNode = document.getElementById("app");
      //   ReactDOM.render(<App />, mountNode);`
      // fs.writeFileSync(indexJsPath, jsTpl)
    // posthtml()
    //     .use(removeTags({tags: ['page']}))
    //     .process(html)
    //     .then(result => {
    //         writeFileSync('output.html', result.html);
    //     });
        // console.log(parser(html)) // Logs a PostHTML AST
    });
    // bundler.on('buildEnd', () => {
    //   // 做一些操作……
    //   console.log('buildEnd')
    // });

    // 编译完成后删除
    if (process.env.NODE_ENV === 'production') {
        bundler.on('bundled', () => {
            setTimeout(() => {
                clean(config.parcelBuildClean);
            }, 0);
        });
    }
};
