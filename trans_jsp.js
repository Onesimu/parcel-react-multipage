const path = require('path');
const CWD = require('process').cwd();
const globby = require('globby');
const rimraf = require('rimraf');
const process = require('process');

// const {readFileSync, writeFileSync} require( 'fs';
const fs = require('fs')
const posthtml = require('posthtml');
const removeTags = require('posthtml-remove-tags');
// const renameTags = require('posthtml-rename-tags')
const renameTags = require('./jsp_rule.js')

const parser = require('posthtml-parser')


const jsp = fs.readFileSync('./src/pages/user/user_center.html', 'utf8');
const html = '<!-- ' + jsp.replace('<!DOCTYPE html>', '--> \n<!DOCTYPE html>')

    posthtml(  [
        // removeTags({tags: ['c:set']}),
        renameTags({
                  'c:if' : 'template', 'c:when': 'template',
                  'c:otherwise': 'template',
                  'c:choose': 'template'
                })
          ])
        .process(html)
        .then(result => {
            fs.writeFileSync('dist/output.html', result.html);
        });
        // console.log(parser(html)) // Logs a PostHTML AST
